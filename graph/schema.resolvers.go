package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"backend/code/challenge/graph/generated"
	"backend/code/challenge/graph/model"
	"context"
)

func (r *mutationResolver) UpdateMeetings(ctx context.Context, input []*model.NewMeeting) (*model.Results, error) {
	result := new(model.Results)
	idMeetingMap := make(map[int]*model.Meeting)
     var meetings []*model.Meeting
     for _, newMeeting := range input {
        updateMeeting := &model.Meeting {
              ID: newMeeting.ID,
              Title: newMeeting.Title,
              Description: newMeeting.Description,
              StartTime: newMeeting.StartTime,
          }
        meetings = append(meetings, updateMeeting)
        idMeetingMap[updateMeeting.ID] = updateMeeting
    }
    for _, meeting := range r.meetings {
        uMeeting, ok := idMeetingMap[meeting.ID]
        if (!ok) {
            result.DeletedID = append(result.DeletedID, meeting.ID)
            continue
        } else if(*uMeeting != *meeting) {
            result.UpdatedID = append(result.UpdatedID, meeting.ID)
        }
        delete(idMeetingMap, meeting.ID)
    }

    for _, meeting := range idMeetingMap {
        result.CreatedID  = append(result.CreatedID, meeting.ID)
    }
    r.meetings = meetings
    return result, nil
}

func (r *queryResolver) Meetings(ctx context.Context) ([]*model.Meeting, error) {
	return r.meetings, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
