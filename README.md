#backend code challenge

## 工具: go gqlgen

启动服务

![run server](screenshot/runServer.png)

打开浏览器界面

![after server run](screenshot/afterServerRun.png)



查询meetings

![query_1](screenshot/query_1.png)



添加两个meeting,并显示created id，updated id和deleted id

![newTwoMeetings](screenshot/newTwoMeetings.png)



此时查询meetings：

![query_2](screenshot/query_2.png)



更新meetings，将第一个meeting的startTime加1，删除第二个meeting，添加id为3的meeting

![updateMeetings](screenshot/updateMeetings.png)



查询meetings

![query_3](screenshot/query_3.png)

